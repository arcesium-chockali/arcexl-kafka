### Checkpoint - Topic, Partition and Offset

* Offset is meaningful only within a partition.
* Order is guaranteed only within a partition - not across partitions.
* Once a data is written to a partition, it cannot be changed - Immutability.
* Data is assigned randomly to a partition, unless a key is provided.

![Topic Representation](topic-representation.png)

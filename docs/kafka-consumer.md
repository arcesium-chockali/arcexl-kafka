
<a name="toc-kafkaConsumer"></a>

Recipe 4 - Consuming messages from Kafka
--------------------------------------------------------------------- 

### Exercise
* Configure UAT profile with `writeStockPriceToKafka` property is set to false
* If your table is not created with `stock_price_pk` constraint, add it now before proceeding with Kafka consumer. Refer the setup recipe.
* Add another implementation for `StockPriceReader` to read from Kafka topic `stockPriceTopic`
     ```
        public interface StockPriceReader {
            List<StockPrice> read();
        }
    ```
* You would need to deserialize the message from kafka to `StockPrice` model. Again, as with Serializer, register `com.fasterxml.jackson.datatype.jsr310.JavaTimeModule` to `ObjectMapper` here as well.
* Print the offset and partition when you consume a message.
* Write a small program that reads from kafka using the above implementation `StockPriceReader` and writes to both DB and Kafka using `StockPriceWriter`
* Test this program by verifying the records at UAT DB - Manual verification would do for now. We will come to adding end-to-end test case later.

### Focus Points 
<details>
<summary>Click here to expand..</summary>

* Rerun this program once again after it is done reading all messages from kafka. Observe what happens. Are you reading all messages in kafka from the beginning? That's not what you want right?
    * Understand `AUTO_OFFSET_RESET_CONFIG` in Consumer Config.
    * Understand `ENABLE_AUTO_COMMIT_CONFIG` in Consumer Config. What is the relevance of this property with respect to this Record and Replay application?
* While writing the KafkaConsumer, you would have stumbled up on `GROUP_ID_CONFIG`. Understand what Consumer groups are.
    * How can your UAT application identify itself as a certain consumer group?
    * How many consumers can you run per group?
    * What happens if there are multiple consumers per group?
    * What does it take for a new application - say `Accounting Engine` to consume these stock prices from stockPriceTopic?

</details>

### Checkpoint - Consumer & Consumer Groups

Click [here](checkpoint-consumer.md)


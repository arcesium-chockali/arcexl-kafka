### FocusPoints/Checkpoint for Guarantees

* There is a reason why I kept stressing on the `stock_price_pk` constraint. This means the producer should never send duplicate StockPrices.
    * You can modify `FeedStockPriceReader` to make atomic read-writes. i.e. every record read from csv reader should be produced into Kafka.
    * Say you have done that too. Is that enough? How do you know that a message you sent to kafka is not duplicated at brokers?
    * Wait, what? Can that even happen?
        * There are various producer guarantees - this is defined by `acks` - acknowledgements.
        * When you send a message to kafka, do you care to get an ack back? if you look for fire and forget - i.e. at most once - then generally this property would be set to 0. This is ideal for log and metrics collectors
        * But our application is a record and replay. So you care about acks. How much?
        * There are 2 other levels
            * acks=1 -> get the ack from the leader of the partition. but replication is not guaranteed (remember in-sync replicas?). If this leader goes down, your message is lost.
            * acks=all -> get ack from leader and replicas. So If you set replication factor set to 5 for a topic, does this mean I block until I get ack from all 5 brokers? Wouldn't a few out of 5 be enough?
                * Exactly. That is why we have `min.insync.replicas` broker properties (which can be overridden at topic properties as well)
                * replication.factor=3, min.insync.replicas=2, acks=all -> this means you can only tolerate one broker going down, otherwise producer will receive an exception on send
        * Is that all? No. Enter "Producer retries"
            * Producer retries when the ack is not received. Say, due to network error, ack never reached the producer but the broker actually received the message. This could cause duplicates.
            ![dups-due-to-network-error](dups-due-to-network-error.png)
        * Note that our app does not care about order. whether price-1 and price-2 reached in the same order at consumer. but if order matters to you, note that even successful retries could mess with the order.
            * When you are sending msg-1 and msg-2, msg-2 succeeds but msg-1 is retried and eventually reaches broker - the order of message received at broker is msg-2, msg-1 eventhough you produced in order - msg-1, msg-2
            * What can you do to prevent this? you need retries but tell producer not to send msg-2 until it received ack for msg-1. This is controlled by `max.in.flight.requests`
        * So how to create an idempotent producer? Just set `enable.idempotence` to true at producer properties. 
        
            ```
            To enable idempotence, the enable.idempotence configuration must be set to true. 
            If set, the retries config will default to Integer.MAX_VALUE and the acks config will default to all. 
            To take advantage of the idempotent producer, it is imperative to avoid application level re-sends since these cannot be de-duplicated.
            As such, if an application enables idempotence, it is recommended to leave the retries config unset, as it will be defaulted to Integer.MAX_VALUE. Additionally, if a send(ProducerRecord) returns an error even with infinite retries (for instance if the message expires in the buffer before being sent), then it is recommended to shut down the producer and check the contents of the last produced message to ensure that it is not duplicated.
            ```
            
            * Producer is intelligent enough to place "idempotent request" - this could be attaching a unique id to messages it retries so that broker can know that it already received the message.
            ![idempotent-producer](idempotent-producer.png)
        * What happens if the KafkaProducer has sent a message and it is persisted to Kafka broker but before the acknowledgement is received by the producer, the producer is killed? 
        Does it not duplicate if it is sent again after the restart?
            
            I cannot explain it any better than [this](https://stackoverflow.com/a/57907663/2256618). Note that the strategy would be different if you had multiple producers. There is also an option of transactional producer. But for our usecase, to save throughput, the simplest thing you can do is to make consumer idempotent. Read on. you will get there.
    * With the acks involved for strong producer guarantees, don't you think we should batch messages at producers? 
        * When multiple records are sent to the same partition, the producer will batch them together. `batch.size` controls the amount of memory in bytes (not messages!) that will be used for each batch.
        * `linger.ms` number of millis producer is willing to wait before sending a batch out - i.e. at the expense of adding a small delay, we are increasing the throughput of the producer.
        * if batch is full (determined by `batch.size`) before the end of `linger.ms` it will be sent to kafka right away.
        * if producer produces faster than the broker, records will be buffered in memory `buffer.memory`. If this buffer is full, send() method blocks. To prevent it from blocking indefinitely, we have `max.block.ms`
        * Let's revisit the point "Having too many partitions per topic"
             * If one increases the number of partitions, message will be accumulated in more partitions in the producer (i.e. `batch.size`). The aggregate amount of memory used may now exceed 
                the configured memory limit. When this happens, the producer has to either block or drop any new message, neither of which is ideal. To prevent this from happening, 
                one will need to reconfigure the producer with a larger memory size.
             * You can read more [here](https://www.confluent.io/blog/how-choose-number-topics-partitions-kafka-cluster/)
    * Note that kafka-client api does a lot behind the scenes to ensure the stock prices are produced into topic to make it available for some UAT application.
        * Would you really like to compromise your production resource (i.e. maintaining buffers for kafka producers, enabling retries, etc) for an UAT usecase?
        * It is a good direction to think. Maybe you want to take these stock prices at non-critical hours and avail it to UAT applications. Think how you can enable that.

* Let's focus on the consumer part now. Assume the upstream never sends you duplicate records. What can cause duplicate records at consumer side?
    * There are 2 consumer commits - sync and async. 
        * When you call consumer.commitSync() it commits the last offset read by consumer.poll() and it blocks until the commit succeeded.
        * commitAsync() as name suggests, is non-blocking.  
        * The drawback is that while commitSync() will retry the commit until it either succeeds or encounters a nonretriable failure, commitAsync() will not retry.
        * But be careful when you put retry logic in commitAsync() callbacks. Imagine that we sent a request to commit offset 2000. There is a temporary communication problem, so the broker never gets the request and therefore never responds. Meanwhile, we processed another batch and successfully committed offset 3000. If commitAsync() now retries 
        the previously failed commit, it might succeed in committing offset 2000 after offset 3000 was already processed and committed. In the case of a rebalance, this will cause more duplicates.
    * Do we even need to call commit explicitly? or rely on auto commits?
        * what if I read 4000 records from poll() and when I was inserting 3000th stock price, auto commit happened (which commits 4000 offset as last read) and uat-app crashes? Next time you poll, you get from 4001 and you lost 3000-4000.
        * So definitely not going to rely on auto-commits
        ![at-most-once-consumer](at-most-once.png)
    * There are apis to explicitly commit offset to a topic and partition.
        * So do we use explicitly commit after writing each record to DB? Note that you are blocking until the offset gets committed to kafka. That's definitely going to slow you down.
        * Even with explicit offset commit, you could write a record to db and die before committing the offset to Kafka. It does not completely solve our problem.
    * In our usecase, when will you commit the last read offset? Ideally when you know for sure that all records returned for the poll() went into uat-db.
        * Write records one by one to uat DB and when all the records returned for that poll() went into uat-db, issue a consumer.commitSync()
        ```
        while(true) {
            ConsumerRecords<String, StockPrice> consumerRecords = kafkaConsumer.poll(Duration.ofSeconds(2));
              for (ConsumerRecord<String, StockPrice> consumerRecord : consumerRecords) {
                  LOGGER.info("Read ConsumerRecord {} , partition {} and offset {} ", consumerRecord.value(), consumerRecord.partition()
                          , consumerRecord.offset());
                  stockPriceWriter.write(consumerRecord.value());
              }
              this.kafkaConsumer.commitSync();
        }
        ```
        Say last committed offset = 1000. poll() returned 500 records - i.e from offset : 1001-1500. 
        What happens when stockPriceWriter.write() failed for the 200th record of this 500 records? i.e. at offset 1200?
        When app comes up, or when this partition gets rebalanced to another consumer, the last committed offset is 1000 and it will write the offset 1001 to 1199 again.
        Hence, stockPriceWriter will fail because of this `stock_price_pk` constraint. 
        
        * To handle these cases, you need to make sure your consumer is idempotent - then tune kafka for at-least once guarantee.
        ![at-least-once-consumer](at-least-once.png)
    * If you make your consumer idempotent, you can even rely on commitAsync()
    * Now make the uat-db writes idempotent.

Conclusion : We expect at-least once guarantee from kafka since our consumers are idempotent.

----------------------------

<ins>Here is another scenario :</ins>

Consumer group A has read a message for a partition from its leader. Now the leader goes down before replicating the message to its replica. So this message, as far as brokers are concerned, is gone.
But consumer group A has read it. If some other consumer group B is reading from the same topic, it won't see this message. Leading to inconsistency.

To prevent this from happening, **not all the data that exists on the leader of the partition is available for clients to read**. Most clients can only read messages that were written to all in-sync replicas.

This behavior also means that if replication between brokers is slow for some reason, it will take longer for new messages to arrive to consumers (since we wait for the messages to replicate first). This delay is limited to replica.lag.time.max.ms—the amount of time a replica can be delayed in replicating new messages while still being considered in-sync.
This also used to be based on the number of messages that a replica is allowed to be lagging behind the leader and still be considered in-sync - `replica.lag.max.messages`. but that [property is removed](https://www.confluent.io/blog/hands-free-kafka-replication-a-lesson-in-operational-simplicity/)

![what can consumer see](watermark.png)

Bonus : Read about `unclean.leader.election.enable` property and understand how you can make a choice between high availability and consistency

### Checkpoint - Consumer & Consumer Groups

* A consumer group can have multiple consumers.
    * Each consumer within a group, reads from exclusive partitions.
    * If you have more consumers than partitions, some consumers will be inactive.
    * Data is read by consumer in order within each partition.
    
    ![Consumer groups](consumer-rebalancing.png)
    
* Kafka stores the offsets at which a consumer group has been reading.
    * This gets stored in topics named `__consumer_offsets`. Go to the `arcExlData/kafka` directory you created under kafka and take a look. You will see consumer_offsets along with `stockPriceTopic`
    * Who commits this offset?
        * if you set `ENABLE_AUTO_COMMIT_CONFIG` to true, offsets are committed by kafka consumer automatically at an interval specified by another property called `AUTO_COMMIT_INTERVAL_MS_CONFIG`
        * if not, you are supposed to explicitly invoke `consumer.commitSync()`. Sync or Async depends on your usecase. How important it is for you to know for sure the offset is committed before proceeding with your application logic.
* `AUTO_OFFSET_RESET_CONFIG` is used when kafka does not have any committed offset for the consumer group id.
    * There can be no committed offset at kafka when 
        * your application went down before the auto-commit kicked off - this is when `ENABLE_AUTO_COMMIT_CONFIG` is set to true.
        * Or when application goes down before it could get to the line where you have issued `consumer.commit`
        * Or when consumer offset itself is deleted by Kafka. Note that the consumer offsets have retention property called `offsets.retention.minutes`. If this is set to, say 1 day, if kafka didn't see a consumer group active for a day, it will clear all the offsets for that group. This could mostly be the case for UAT/Dev application consumers since 1 day downtime is quite possible. 
    * So when consumer starts, it needs to tell kafka how to reset offset, when there is no offset information for its consumer group. This can be :
        * earliest = read from the start of the log
        * latest = read from the end of the log. i.e. all messages that came in after the consumer subscribed to the topic
        * none = throw exception when no offset is found
* Multiple consumer groups can be reading from a topic. In that case, consumer offset for that topic is stored at Kafka for each consumer group.

![Multiple consumer groups](new-consumer-group.png)
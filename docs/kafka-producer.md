
<a name="toc-kafkaProducer"></a>
## Recipe 3 - Producing messages into Kafka

### Exercise
* Make sure stockPriceTopic is created with 3 partitions.
* Add an implementation for `StockPriceWriter` interface

    ```
    public interface StockPriceWriter {
        void writeStockPrice(StockPrice stockPrice);
    }
    ```
* This implementation should write to DB and then to Kafka using Kafka Producer API
* Also, it should only write to Kafka if `writeStockPriceToKafka` property is set to true
* At prod profile, set `writeStockPriceToKafka` to true and at uat profile, set this to false
* Add an implementation for `StockPriceReader` interface. This implementation should read stockPrice from [feeds.csv](../record-replay/src/main/resources/feeds.csv) file. 

    ```
    public interface StockPriceReader {
        List<StockPrice> read();
    }
    ```
    * Do not waste too much time on this implementation. You need opencsv - version 5 or above - in order to parse dates to LocalDate. Copy the implementation from [FeedStockPriceReaderImpl](../record-replay/src/main/java/com/arcexl/reader/FeedStockPriceReader.java). You will also need few annotations at the [StockPrice](../record-replay/src/main/kotlin/com/arcexl/domain/StockPrice.kt) model.
* Write a small program that reads from CSV using the `StockPriceReader` and writes to both DB and Kafka using `StockPriceWriter`
* Use CLI commands to verify the kafka producer
    * `kafka-console-consumer` CLI to verify that the messages are produced
    
        `bin\windows\kafka-console-consumer.bat --topic stockPriceTopic --bootstrap-server 127.0.0.1:9092 --from-beginning`
    
    * While the console-consumer is still active - i.e. don't kill it yet. Run consumer-groups
    
        `bin\windows\kafka-consumer-groups.bat --bootstrap-server 127.0.0.1:9092 --list`
    
    * In the list above, pick the one with groupId like "console-consumer-%"
    
        `bin\windows\kafka-consumer-groups.bat --bootstrap-server 127.0.0.1:9092 --group console-consumer-18941 --describe`
    
    * Observe that consumer group is "aware" of partitions within a topic. That is the reason why, in consumer, 
    IBM gets displayed in order - i.e. starting from 2010 to 2020. i.e. within partition, the data is guaranteed to be in inserted order 
### Focus Points

<details>
<summary>click here to expand</summary>
  
* Understand synchronous and asynchronous Kafka producer API.
* You would need Serializer and Deserializer for StockPrice. You would definitely run into issues with deserializing LocalDate. Register `com.fasterxml.jackson.datatype.jsr310.JavaTimeModule` to `ObjectMapper`
* Add a callback and log the metadata - The message that you produced, which partition did it go to? and what is the offset?
* First produce the messages without key.
* Produce messages with key. Observe that messages with the same key goes to the same partition.
* Observe that offset is increasing within the partition.
* Are you wondering what happens when a price written to DB did not make it to Kafka? Then you are going in the right direction. Put that thought on hold for a little while. We will get there soon.

</details>

### Checkpoint - Topic, Partition and Offset

Click [here](checkpoint-producer.md)

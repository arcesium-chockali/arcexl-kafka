<a name="toc-scaling"></a>
## Recipe 5 - Scaling consumers

### Exercise
* Modify your application to be a long running - i.e. have the reader-writer continuously run.
* At this point, you should have prod and uat profile setup. 
    * Prod application should be reading from CSV and write to arcexl_prod db and kafka's stockPriceTopic
    * Uat application should be reading from kafka's stockPriceTopic and write to arcexl_uat db
* Add delay at writer when you are writing to kafka and reading from kafka.
    * say prod application takes 3 seconds to produce 1 stock price 
    * and uat application takes 20 seconds to consume a batch of stock price. You can also limit the batch size using `MAX_POLL_RECORDS_CONFIG`
* Let's scale consumer now. Run Uat application -1  and after some time, run another instance of this uat application -2
    * Observe the logs at uat-1 to see how consumer rebalancing happens.
    * `stockPriceTopic` was created with 3 partitions. We have 2 consumers for this topic. You should see one of the uat app reading from 2 partitions and other from just 1 partition.

### FocusPoints

<details>
<summary>Click here to expand</summary>

* We could have let multiple threads poll from consumer right? Why didn't we do that? 
    * Kafka consumers are not thread safe. One consumer per thread is the rule. As long as each thread possess its own KafkaConsumer, we are good. Try that out as an exercise!
* You would have already used `consumer.poll()` what is the parameter you pass to this poll()?
    * How can you control the number of records returned from kafka for each poll? psst! you already know this
* How does consumer establish liveliness with brokers?
    * Note that a consumer can add to a group, leave a group, stay in a group but not consume anything.
    * In this exercise, if uat-app-2 is killed, you would see all 3 partitioned assigned to uat-app-1. How/When does kafka know that a consumer died and that it needs to rebalance?  
* What if there are too many messages for a topic that a single poll() at consumer brings down the uat-application due to sheer volume?
    * What happens if your uat-app is not only reading from `stockPriceTopic` but also reading from another topic, say, `manualPriceTopic`? and this manualPriceTopic has 10x more partitions and 100x more messages per partition than `stockPriceTopic`?
* What if there are no messages for a topic? Do we poll less frequently? If we don't poll often, can kafka think this consumer is dead?
* Can you delete partitions from topic?
* Can you scale by increasing partitions in a topic?
    * Try altering partitions for `stockPriceTopic` and observe the CLI output.
    
        `bin\windows\kafka-topics.bat --zookeeper 127.0.0.1:2181 --topic stockPriceTopic --alter --partitions 6`
* How do you derive the number of partitions to be set per topic? Here are few questions to ask:
    * What is the throughput you expect to achieve for the topic? For example, do you expect to write 100 KB per second or 1 GB per second?
    * What is the maximum throughput you expect to achieve when consuming from a single partition? A partition will always be consumed completely by a single consumer.
    * If you know that your slower consumer writes the data to a database and this database never handles more than 50 MB per second from each thread writing to it, then you know you are limited to 50 MB/sec throughput when consuming from a partition.
    * So if I want to be able to write and read 1 GB/sec from a topic, and I know each consumer can only process 50 MB/s, then I know I need at least 20 partitions. This way, I can have 20 consumers reading from the topic and achieve 1 GB/sec.
    
* In this particular usecase, if you keep creating more uat instances, at certain point your local postgres would have complained "max connections reached". This is a nice reminder that scaling the application alone is not enough. Here your Uat-DB would become a bottleneck.
</details>
 
### Checkpoint - Other consumer properties

Click [here](checkpoint-scaling.md)
<a name="toc-guarantees"></a>
## Recipe 6 - Kafka guarantees

* How can you enforce Strong Producer guarantees? - i.e. messages that an application produce, get into the broker.
 
    ***All*** messages produced by the production instance must reach kafka broker
 
* How can you enforce an idempotent producer? - i.e. producer.send() does not end up with duplicate messages produced at broker

  All messages produced by the production instance must reach kafka broker ***once***
  
* How can you enforce at-least once consumer guarantees?

    ***All*** messages read from kafka gets into uat DB

### FocusPoints/Checkpoint

Click [here](cf-guarantees.md)

[Link](https://drive.google.com/file/d/1cj0ad_CiliUPt5ct_iODbxbOF6zKSNSU/view?usp=sharing) to the lecture

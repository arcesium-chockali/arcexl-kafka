# Kafka - Level 0
This is recipe based training on Apache Kafka for ArcExL. Level-0 covers basic concepts of Kafka. KStreams and KConnect are out of scope for this training. 

## Overview

Here is a brief overview of what we are going to build.

![Record-and-Replay](docs/record_and_replay.png)

1. StockPriceApp-Prod is going to read stock prices from Feeds - which is actually a csv [committed to the repo](record-replay/src/main/resources/feeds.csv)
1. StockPriceAdd-Prod writes these stock prices to prod-DB
1. It also writes to stockPriceTopic at Kafka
1. StockPriceApp-Uat is reading the stock prices from stockPriceTopic at Kafka
1. StockPriceApp-Uat writes the stock prices read from kafka to uat-DB

-------------

## How the course is aligned

* The incremental steps needed to build the application is modeled as **Recipe**
* Each recipe has **Focus Points** - i.e. what you should learn out of building that recipe.
* After every recipe or bunch of recipes, we have **Checkpoints** - i.e. Summary of everything you know about Kafka up until now. 
We stop at each Checkpoint, drive the points home and THEN move on to further recipes. Because understanding these checkpoints are crucial for recipes that follow.

----------------------------------

## Table of contents
1. Prerequisites
    1. [Development Environment](docs/development-env.md)
    1. [Project Setup](docs/project-setup.md)
    1. [Kafka Setup](docs/kafka-setup.md)
1. [Kafka Producer](docs/kafka-producer.md)
1. [Kafka Consumer](docs/kafka-consumer.md)
1. [Scaling](docs/scaling.md)
1. [Guarantees](docs/guarantees.md)
1. [Retention](docs/retention.md)

------------------------------------
## References

1. [Kafka - The definitive guide](https://www.confluent.io/resources/kafka-the-definitive-guide/)
1. [Kafka - Learning Series](https://learning.oreilly.com/videos/apache-kafka-series/9781789342604)
1. As always, [official documentation](https://kafka.apache.org/documentation/) - Make sure you are referring to the documentation for the Kafka broker version you are using.
1. The entire course gave little importance to monitoring, security, cluster setup, kafka admin operations. These could be your next steps.
1. Also notice how I conveniently left kafka transactions out of the picture :wink: TBH I couldn't fit it into Level 0 but there are plenty of resources to learn `what` is exactly-once guarantee in Kafka and `how` it works. After acing THIS much of Kafka fundamentals, it should be fairly easy to grasp the concept. Do give it a try.
